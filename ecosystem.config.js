module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps: [
    // First application
    {
      name: "API",
      script: "index.js",
      env_production: {
        NODE_ENV: "production"
      }
    }
  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy: {
    production: {
      user: "root",
      host: "157.230.33.223",
      ref: "origin/master",
      repo: "https://gitlab.com/duylinh196tb/test-ci.git",
      path: "",
      "post-setup":
        "npm install; pm2 start ecosystem.config.json --env production",
      "post-deploy":
        "npm install && pm2 reload ecosystem.config.js --env production"
    }
  }
};
